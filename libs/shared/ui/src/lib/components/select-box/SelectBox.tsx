import { useState } from 'react';

export interface SelectBoxProps {
  items: {
    id: string | number;
    label: string;
  }[];
  value?: string | number;
  placeholder?: string;
  selectItem: (text: string | number) => void;
}

export function SelectBox(props: SelectBoxProps) {
  const [items, setItem] = useState(props.items);
  const [selectedItem, setSelectedItem] = useState<string | number | null>(
    null
  );

  const handleItemClick = (id: string | number) => {
    selectedItem === id ? setSelectedItem(null) : setSelectedItem(id);
    props.selectItem(id);
  };

  return (
    <>
      <label
        htmlFor="default"
        className="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-400"
      >
        {props.placeholder}
      </label>
      <select
        onChange={(event) => handleItemClick(event.target.value)}
        value={props.value}
        className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 capitalize"
      >
        {items.map((item) => (
          <option
            className="capitalize"
            value={item.id}
            key={item.id}
          >
            {item.label}
          </option>
        ))}
      </select>
    </>
  );
}

export default SelectBox;
