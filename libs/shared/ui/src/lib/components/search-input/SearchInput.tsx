import { ChangeEvent, useEffect, useState, memo } from 'react';
import { useDebounce } from '@personio/recruitment-data';

export interface SearchInputProps {
  defaultValue: string;
  onInputChange: (text: string) => void;
  debounceTime?: number;
  disableSearch?: boolean;
}

export function SearchInput({
  disableSearch,
  onInputChange,
  defaultValue = '',
  debounceTime = 400,
}: SearchInputProps) {
  const [searchText, setSearch] = useState(defaultValue);
  const debouncedValue = useDebounce(searchText, debounceTime);

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    if (disableSearch) return;

    console.log('Handle change', event.target.value);
    setSearch(event.target.value);
  };

  useEffect(() => {
    if (disableSearch) return;

    onInputChange(debouncedValue);
  }, [debouncedValue, onInputChange, disableSearch]);

  return (
    <>
      <label htmlFor="input-search" className="sr-only">
        Search
      </label>
      <div className="relative">
        <div className="absolute inset-y-0 left-0 flex items-center pl-3 pointer-events-none">
          <svg
            className="w-5 h-5 text-gray-500 dark:text-gray-400"
            fill="currentColor"
            viewBox="0 0 20 20"
            xmlns="http://www.w3.org/2000/svg"
          >
            <path
              fillRule="evenodd"
              d="M8 4a4 4 0 100 8 4 4 0 000-8zM2 8a6 6 0 1110.89 3.476l4.817 4.817a1 1 0 01-1.414 1.414l-4.816-4.816A6 6 0 012 8z"
              clipRule="evenodd"
            />
          </svg>
        </div>
        <input
          id="input-search"
          type="text"
          placeholder="Search by name"
          value={searchText}
          className="focus:outline-none bg-gray-50 dark:bg-gray-700 text-sm rounded-lg block w-80 pl-10 p-2.5 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
          disabled={disableSearch}
          onChange={handleChange}
        />
      </div>
    </>
  );
}

export default SearchInput;
