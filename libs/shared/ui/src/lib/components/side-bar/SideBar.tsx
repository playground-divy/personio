import { useRouter } from 'next/router';
import { reset, useAppDispatch } from '@personio/recruitment-data';

export function SideBar() {
  const router = useRouter();
  const dispatch = useAppDispatch();

  function handleClick() {
    dispatch(reset());
    router.push('/dashboard');
  }

  return (
    <aside
      className="w-64 fixed left-0 top-0 h-screen bg-slate-700 pt-10 px-4"
      aria-label="Sidebar"
    >
      <div className="overflow-y-auto">
        <ul className="space-y-2">
          <li>
            <button
              className="flex items-center text-white text-base font-normal text-gray-900 rounded-lg dark:text-white"
              type="button"
              onClick={handleClick}
            >
              <svg
                className="w-6 h-6 text-white transition duration-75 dark:text-gray-400 group-hover:text-gray-900 dark:group-hover:text-white"
                fill="currentColor"
                viewBox="0 0 20 20"
                xmlns="http://www.w3.org/2000/svg"
              >
                <path d="M2 10a8 8 0 018-8v8h8a8 8 0 11-16 0z" />
                <path d="M12 2.252A8.014 8.014 0 0117.748 8H12V2.252z" />
              </svg>
              <span className="ml-3 text-white">Dashboard</span>
            </button>
          </li>
        </ul>
      </div>
    </aside>
  );
}

export default SideBar;
