import TableHead from './TableHead';
import { useSortableTable } from './useSortableTable';
import { LoadingStatus } from '@personio/recruitment-data';
import { Loader } from '@personio/libs-shared-ui';
import TableBody from './TableBody';

export interface TableUiColumn {
  label: string;
  accessor: string;
  sortable: boolean;
  sortbyOrder?: string;
}

export interface TableProps {
  data: Array<{ [key: string]: string }>;
  columns: Array<TableUiColumn>;
  loadingStatus: LoadingStatus;
  disableSorting?: boolean;
}

export const TableUi = ({
  data,
  columns,
  loadingStatus,
  disableSorting = false,
}: TableProps) => {
  const [tableData, handleSorting] = useSortableTable(data, columns);

  return (
    <div>
      <table className="table-auto w-full text-sm text-left text-gray-500 dark:text-gray-400">
        <TableHead
          columns={columns}
          disableSorting={disableSorting}
          handleSorting={handleSorting}
        />
        {loadingStatus === LoadingStatus.SUCCEEDED ? (
          <TableBody
            columns={columns}
            tableData={tableData as Array<{ [key: string]: string }>}
          />
        ) : (
          ''
        )}
      </table>
      {loadingStatus === LoadingStatus.LOADING ? (
        <div className="pb-10 pt-10 text-center w-full flex justify-center min-h-[65vh] flex justify-center items-center">
          <Loader />
        </div>
      ) : (
        ''
      )}
      {loadingStatus === LoadingStatus.FAILED ? (
        <div className="pb-10 pt-10 text-center w-full flex justify-center min-h-[65vh] flex justify-center items-center">
          <h3 className="h-5 font-semibold">
            Something went wrong in processing your request.
          </h3>
        </div>
      ) : (
        ''
      )}
    </div>
  );
};

export default TableUi;
