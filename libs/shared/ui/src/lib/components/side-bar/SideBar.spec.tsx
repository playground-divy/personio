import { render } from '@testing-library/react';
import SideBar from './SideBar';

describe('SideBar', () => {
  it('should render successfully with link to dashboard page', () => {
    const baseElement = render(<SideBar />);
    expect(
      baseElement
        .getByText('Dashboard')
        .closest('a')
        ?.href.includes('/dashboard')
    ).toBeTruthy();
  });
});
