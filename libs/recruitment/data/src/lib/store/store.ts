import { configureStore } from '@reduxjs/toolkit';
import candidatesReducer from '../reducers/candidatesSlice';

export const store = configureStore({
  reducer: {
    candidates: candidatesReducer,
  },
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>
// Inferred type: {candidates: CandidatesStore}
export type AppDispatch = typeof store.dispatch
