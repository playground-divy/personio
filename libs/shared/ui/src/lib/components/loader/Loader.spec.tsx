import { render } from '@testing-library/react';
import { Loader } from '@personio/libs-shared-ui';

describe('Loader', () => {
  it('should render the loader', () => {
    const { baseElement } = render(<Loader />);
    expect(baseElement).not.toBeNull();
  });
});
