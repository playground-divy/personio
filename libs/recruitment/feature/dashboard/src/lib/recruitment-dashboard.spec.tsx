import { render } from '@testing-library/react';

import RecruitmentFeatureDashboard from './recruitment-dashboard';

describe('RecruitmentFeatureDashboard', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<RecruitmentFeatureDashboard />);
    expect(baseElement).toBeTruthy();
  });
});
