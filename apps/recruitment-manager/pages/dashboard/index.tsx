import { RecruitmentDashboard } from '@personio/recruitment-feature-dashboard';
import { SideBar } from '@personio/libs-shared-ui';
import { useRouter } from 'next/router';
import { useCallback, useEffect } from 'react';
import {
  CandidatesStore,
  filterByJobType,
  filterByName,
  filterByStatus,
  LoadingStatus,
  updateQueryParams,
  useAppDispatch,
} from '@personio/recruitment-data';
import { useSelector } from 'react-redux';

export function Index() {
  const dispatch = useAppDispatch();
  const router = useRouter();
  const { name, status, jobType } = router.query;
  const loadingStatus = useSelector(
    (state: { candidates: CandidatesStore }) => state.candidates.loadingStatus
  );
  const filters = useSelector(
    (state: { candidates: CandidatesStore }) => state.candidates.filters
  );

  const addQueryParams = useCallback(() => {
    if (loadingStatus !== LoadingStatus.SUCCEEDED) {
      return;
    }

    // Set query params
    if (router.query) {
      dispatch(updateQueryParams(JSON.stringify(router.query || {})));
    }

    if (status) {
      dispatch(filterByStatus(status));
    }

    if (jobType) {
      dispatch(filterByJobType(jobType));
    }

    if (name) {
      dispatch(filterByName(name));
    }
  }, [loadingStatus]);

  useEffect(() => {
    addQueryParams();
  }, [addQueryParams]);

  return (
    <div className="flex h-full">
      <div className="flex w-72 h-full">
        <SideBar />
      </div>
      <div className="flex flex-col w-full bg-white px-10 overflow-x-hidden overflow-y-auto">
        <RecruitmentDashboard />
      </div>
    </div>
  );
}

export default Index;
