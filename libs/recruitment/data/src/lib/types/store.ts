import { Candidates } from './candidates';

export enum LoadingStatus {
  IDLE = 'idle',
  LOADING = 'loading',
  SUCCEEDED = 'succeeded',
  FAILED = 'failed',
}

export interface FilterCandidate {
  name: string;
  status: string;
  jobType: string;
}

export interface SortCandidate {
  direction: 'asc' | 'desc';
  type: string;
}

export interface Pagination {
  page: number;
  pageSize: number;
  totalCount: number;
}

export interface CandidatesStore {
  candidates: Array<Candidates>;
  loadingStatus: LoadingStatus;
  filters: FilterCandidate;
  sort: SortCandidate;
  pagination: Pagination;
  queryParams: string;
}
