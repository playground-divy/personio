import { useEffect, useState } from 'react';

import {
  ApplicationStatus,
  CandidatesStore,
  filterByJobType,
  filterByName,
  filterByStatus,
  JobDescription,
  useAppDispatch,
} from '@personio/recruitment-data';
import { useSelector } from 'react-redux';
import { SelectBox, SearchInput } from '@personio/libs-shared-ui';

export interface FilterProps {
  name?: string;
  status?: string;
  jobType?: string;
  disableFilters?: boolean;
}

export function Filters(props: FilterProps) {
  const { disableFilters } = props;
  const [searchText, setSearch] = useState(props.name);
  const [showFilters, setShowFilters] = useState(false);
  const queryParams = useSelector(
    (state: { candidates: CandidatesStore }) => state.candidates.queryParams
  );
  const dispatch = useAppDispatch();
  const statuses = Object.values(ApplicationStatus).map((status) => ({
    id: status,
    label: status,
  }));
  const jobPositions = Object.values(JobDescription).map((jobType) => ({
    id: jobType,
    label: jobType,
  }));

  useEffect(() => {
    if (queryParams) {
      const addedFilters = JSON.parse(queryParams);

      if ((addedFilters && addedFilters?.status) || addedFilters?.jobType) {
        setShowFilters(true);
      }
    }
  }, [queryParams, setShowFilters]);

  useEffect(() => {
    if (disableFilters) {
      return;
    }

    dispatch(filterByName(searchText));
  }, [dispatch, searchText]);

  return (
    <>
      <div
        className={`flex bg-gray-50 dark:bg-gray-700 border dark:border-gray-700 items-center`}
      >
        <button
          type="button"
          onClick={() => setShowFilters(!showFilters)}
          disabled={props.disableFilters}
          className="border-r text-gray-900 bg-gray-50 dark:bg-gray-700 hover:bg-gray-200 font-medium text-sm px-3 py-3 text-center flex items-center dark:focus:ring-gray-500"
        >
          <span className="px-3">Filters</span>
          {!showFilters ? (
            <svg
              width="20"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
            >
              <path
                strokeWidth="1"
                fill="#666666"
                stroke="#666666"
                d="M10.168 12.136l-6.485-6.13c-.3-.285-.775-.27-1.06.03-.285.3-.27.775.03 1.06l7.515 7.104 7.515-7.104c.3-.285.315-.76.03-1.06-.285-.3-.76-.315-1.06-.03l-6.485 6.13z"
              />
            </svg>
          ) : (
            <svg
              width="20"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 20 20"
            >
              <path
                strokeWidth="1"
                fill="#666666"
                stroke="#666666"
                d="M10.168 7.864l-6.485 6.13c-.3.285-.775.27-1.06-.03-.285-.3-.27-.775.03-1.06L10.168 5.8l7.515 7.104c.3.285.315.76.03 1.06-.285.3-.76.315-1.06.03l-6.485-6.13z"
              />
            </svg>
          )}
        </button>
        <div>
          <SearchInput
            defaultValue={props.name as string}
            debounceTime={400}
            onInputChange={(value) => setSearch(value)}
            disableSearch={props.disableFilters}
          />
        </div>
      </div>

      {showFilters ? (
        <div className="flex bg-gray-100 border-b border-l border-r px-10 py-5">
          <div className="w-72 pr-6">
            <SelectBox
              items={statuses}
              value={props.status}
              placeholder="Application status"
              selectItem={(key: string | number) =>
                dispatch(filterByStatus(key))
              }
            />
          </div>
          <div className="w-72 pr-6">
            <SelectBox
              items={jobPositions}
              value={props.jobType}
              placeholder="Positions applied"
              selectItem={(key: string | number) =>
                dispatch(filterByJobType(key))
              }
            />
          </div>
        </div>
      ) : null}
    </>
  );
}

export default Filters;
