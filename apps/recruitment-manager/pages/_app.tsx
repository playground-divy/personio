import { AppProps } from 'next/app';
import Head from 'next/head';
import { store } from '@personio/recruitment-data';
import { Provider } from 'react-redux';
import '../public/styles.css';

function CustomApp({ Component, pageProps }: AppProps) {
  return (
    <Provider store={store}>
      <Head>
        <title>Welcome to recruitment-manager!</title>
      </Head>
      <main className="app">
        <Component {...pageProps} />
      </main>
    </Provider>
  );
}

export default CustomApp;
