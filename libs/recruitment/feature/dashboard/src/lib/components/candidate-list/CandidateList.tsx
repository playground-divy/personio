import {
  Candidates,
  CandidatesStore,
  LoadingStatus,
} from '@personio/recruitment-data';
import { useSelector } from 'react-redux';
import { useState } from 'react';
import { Pagination, TableUi, TableUiColumn } from '@personio/libs-shared-ui';

export interface CandidateListProps {
  candidates: Candidates[];
}

export function CandidateList(props: CandidateListProps) {
  const PAGE_SIZE = 10;
  const candidates = props.candidates || [];
  const [currentPage, setCurrentPage] = useState(1);
  const [startIndex, setStartIndex] = useState(0);
  const [endIndex, setEndIndex] = useState(10);
  const loadingStatus = useSelector(
    (state: { candidates: CandidatesStore }) => state.candidates.loadingStatus
  );
  const totalCount = useSelector(
    (state: { candidates: CandidatesStore }) =>
      state.candidates.pagination.totalCount
  );
  const onPageChange = (pageNumber: number) => {
    const startIndex = pageNumber === 1 ? 0 : pageNumber * PAGE_SIZE;
    const endIndex =
      (pageNumber === 1 ? pageNumber : pageNumber + 1) * PAGE_SIZE;

    setStartIndex(startIndex);
    setEndIndex(endIndex);
    setCurrentPage(pageNumber);
  };

  const columns: TableUiColumn[] = [
    { label: 'Name', accessor: 'name', sortable: false },
    { label: 'Email', accessor: 'email', sortable: false },
    { label: 'Age', accessor: 'birth_date', sortable: false },
    {
      label: 'Years of Experience',
      accessor: 'year_of_experience',
      sortable: true,
      sortbyOrder: 'asc',
    },
    {
      label: 'Position applied',
      accessor: 'position_applied',
      sortable: true,
      sortbyOrder: 'asc',
    },
    {
      label: 'Applied',
      accessor: 'application_date',
      sortable: true,
      sortbyOrder: 'asc',
    },
    { label: 'Status', accessor: 'status', sortable: false },
  ];

  return (
    <>
      <TableUi
        data={(candidates as unknown as Array<{ [key: string]: string }>).slice(
          startIndex,
          endIndex
        )}
        columns={columns}
        loadingStatus={loadingStatus}
        disableSorting={loadingStatus === LoadingStatus.LOADING}
      />
      <div className="items-center flex justify-center py-3">
        <Pagination
          pageSize={PAGE_SIZE}
          totalCount={totalCount - PAGE_SIZE}
          currentPage={currentPage}
          onPageChange={onPageChange}
        />
      </div>
    </>
  );
}

export default CandidateList;
