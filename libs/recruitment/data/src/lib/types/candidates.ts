import exp from 'constants';

export enum ApplicationStatus {
  ALL = 'all',
  REJECTED = 'rejected',
  WAITING = 'waiting',
  APPROVED = 'approved',
}

export interface Candidates {
  id: number;
  name: string;
  email: string;
  birth_date: string;
  year_of_experience: number;
  position_applied: string;
  application_date: string;
  status: ApplicationStatus;
}

export enum JobDescription {
  ALL = 'all',
  DESIGNER = 'designer',
  PLANNER = 'planner',
  PRODUCER = 'producer',
  ORCHESTRATOR = 'orchestrator',
  ANALYST = 'analyst',
  ADMINISTRATOR = 'administrator',
  STRATEGIST = 'strategist',
  TECHNICIAN = 'technician',
  LIAISON = 'liaison',
  ASSOCIATE = 'associate',
  ASSISTANT = 'assistant',
  REPRESENTATIVE = 'representative',
  DEVELOPER = 'developer',
  AGENT = 'agent',
  MANAGER = 'manager',
  EXECUTIVE = 'executive',
  COORDINATOR = 'coordinator',
  DIRECTOR = 'director',
  FACILITATOR = 'facilitator',
  ARCHITECT = 'architect',
  OFFICER = 'officer',
  CONSULTANT = 'consultant',
  SUPERVISOR = 'supervisor',
  ENGINEER = 'engineer',
  SPECIALIST = 'specialist',
}
