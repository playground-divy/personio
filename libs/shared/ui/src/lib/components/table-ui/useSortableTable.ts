import { useEffect, useState } from 'react';
import { TableUiColumn } from './TableUi';

function getDefaultSorting(
  defaultTableData: Array<{ [key: string]: string }>,
  columns: Array<TableUiColumn>
) {
  return [...defaultTableData].sort((a, b) => {
    const filterColumn = columns.filter((column) => column.sortbyOrder);

    // Merge all array objects into single object and extract accessor and sortbyOrder keys
    const { accessor = 'id', sortbyOrder = 'asc' } = Object.assign(
      {},
      ...filterColumn
    );

    if (a[accessor] === null) return 1;
    if (b[accessor] === null) return -1;
    if (a[accessor] === null && b[accessor] === null) return 0;

    const ascending = a[accessor]
      .toString()
      .localeCompare(b[accessor].toString(), 'en', {
        numeric: true,
      });

    return sortbyOrder === 'asc' ? ascending : -ascending;
  });
}

export const useSortableTable = (
  data: Array<{ [key: string]: string }>,
  columns: Array<TableUiColumn>
) => {
  const [tableData, setTableData] = useState<Array<{ [key: string]: string }>>(
    []
  );

  useEffect(() => {
    setTableData(getDefaultSorting(data, columns));
  }, [setTableData, columns, data]);

  const handleSorting = (sortField: string, sortOrder: string) => {
    if (sortField) {
      const sorted = [...tableData].sort((a, b) => {
        if (a[sortField] === null) return 1;
        if (b[sortField] === null) return -1;
        if (a[sortField] === null && b[sortField] === null) return 0;
        return (
          a[sortField].toString().localeCompare(b[sortField].toString(), 'en', {
            numeric: true,
          }) * (sortOrder === 'asc' ? 1 : -1)
        );
      });
      setTableData(sorted);
    }
  };

  return [tableData, handleSorting];
};
