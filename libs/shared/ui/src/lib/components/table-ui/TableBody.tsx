import { TableUiColumn } from './TableUi';

interface TableProps {
  tableData: Array<{ [key: string]: string }>;
  columns: Array<TableUiColumn>;
}

const TableBody = ({ tableData, columns }: TableProps) => {
  return (
    <tbody>
      {tableData.map((data) => {
        return (
          <tr
            key={data['id']}
            className="bg-white border-b dark:bg-gray-800 dark:border-gray-700"
          >
            {columns.map(({ accessor }) => {
              const tData = data[accessor] ? data[accessor] : '——';
              return (
                <td key={accessor} className="px-6 py-4">
                  {tData}
                </td>
              );
            })}
          </tr>
        );
      })}
    </tbody>
  );
};

export default TableBody;
