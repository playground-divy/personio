import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';
import { CandidatesStore, LoadingStatus } from '../types';
import axios from 'axios';
import { useDispatch } from 'react-redux';
import { AppDispatch } from '../store';

export const initialState: CandidatesStore = {
  candidates: [],
  loadingStatus: LoadingStatus.IDLE,
  filters: {
    name: '',
    status: '',
    jobType: '',
  },
  sort: {
    direction: 'asc',
    type: '',
  },
  pagination: {
    page: 1,
    pageSize: 10,
    totalCount: 0,
  },
  queryParams: '',
};

export const fetchCandidates = createAsyncThunk(
  'candidates/fetchCandidates',
  () => {
    return axios
      .get('https://personio-fe-test.herokuapp.com/api/v1/candidates')
      .then((response) => response.data.data);
  }
);

export const candidatesSlice = createSlice({
  name: 'candidates',
  initialState,
  reducers: {
    filterByName: (state, action) => {
      state.filters.name = action.payload;
    },
    filterByStatus: (state, action) => {
      state.filters.status = action.payload;
    },
    filterByJobType: (state, action) => {
      state.filters.jobType = action.payload;
    },
    sortListBy: (state, action) => {
      state.sort = action.payload;
    },
    updatePageNumber: (state, action) => {
      state.pagination.page = action.payload;
    },
    updateTotalCount: (state, action) => {
      state.pagination.totalCount = action.payload;
    },
    updateQueryParams: (state, action) => {
      state.queryParams = action.payload;
    },
    reset: () => {
      return {
        ...initialState,
      };
    },
  },
  extraReducers(builder) {
    builder
      .addCase(fetchCandidates.pending, (state) => {
        state.loadingStatus = LoadingStatus.LOADING;
      })
      .addCase(fetchCandidates.rejected, (state) => {
        state.loadingStatus = LoadingStatus.FAILED;
        state.candidates = [];
      })
      .addCase(fetchCandidates.fulfilled, (state, action) => {
        const payload = action.payload;

        state.loadingStatus =
          payload === undefined
            ? LoadingStatus.FAILED
            : LoadingStatus.SUCCEEDED;
        state.candidates = payload === undefined ? [] : payload;
      });
  },
});

export const {
  filterByName,
  filterByStatus,
  filterByJobType,
  sortListBy,
  updateTotalCount,
  updatePageNumber,
  updateQueryParams,
  reset,
} = candidatesSlice.actions;

export const useAppDispatch = () => useDispatch<AppDispatch>();
export default candidatesSlice.reducer;
