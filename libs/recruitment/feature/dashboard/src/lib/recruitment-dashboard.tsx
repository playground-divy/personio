import { useSelector } from 'react-redux';
import {
  Candidates,
  CandidatesStore,
  fetchCandidates,
  LoadingStatus,
  updateTotalCount,
  useAppDispatch,
} from '@personio/recruitment-data';
import { useCallback, useEffect, useState } from 'react';
import Filters from './components/filters/Filters';
import CandidateList from './components/candidate-list/CandidateList';
import { useRouter } from 'next/router';

export function RecruitmentDashboard() {
  const dispatch = useAppDispatch();
  const router = useRouter();
  const [filteredCandidates, setCandidates] = useState<Candidates[]>([]);
  const candidates = useSelector(
    (state: { candidates: CandidatesStore }) => state.candidates.candidates
  );
  const loadingStatus = useSelector(
    (state: { candidates: CandidatesStore }) => state.candidates.loadingStatus
  );
  const filters = useSelector(
    (state: { candidates: CandidatesStore }) => state.candidates.filters
  );

  const updateRouteParams = useCallback(() => {
    const filterParams: { [key: string]: string } = { ...filters };
    const validFilters: { [key: string]: string } = {};

    if (loadingStatus !== LoadingStatus.SUCCEEDED) {
      return;
    }

    // Set the router params.
    Object.keys(filterParams).forEach((key: string) => {
      if (filterParams[key]) {
        validFilters[key] = filterParams[key];
      }
    });

    router.push(
      {
        pathname: '/dashboard',
        query: { ...validFilters },
      },
      undefined,
      {
        shallow: true,
      }
    );
  }, [filters]);

  useEffect(() => {
    if (loadingStatus === LoadingStatus.IDLE) {
      dispatch(fetchCandidates());
    }
  }, [loadingStatus, dispatch]);

  useEffect(() => {
    let filteredResults: Candidates[] = [];

    if (candidates) {
      filteredResults = [...candidates];
    }

    if (filters.name) {
      filteredResults = filteredResults.filter((candidate) =>
        candidate.name.toLowerCase().includes(filters.name.toLowerCase())
      );
    }

    if (filters.status) {
      filteredResults = filteredResults.filter((candidate) =>
        filters.status === 'all' ? true : candidate.status === filters.status
      );
    }

    if (filters.jobType) {
      filteredResults = filteredResults.filter((candidate) =>
        filters.jobType === 'all'
          ? true
          : candidate.position_applied.toLowerCase() ===
            filters.jobType.toLowerCase()
      );
    }

    setCandidates(filteredResults);
    dispatch(updateTotalCount(filteredResults.length));
    updateRouteParams();
  }, [filters, candidates, dispatch, updateRouteParams]);

  return (
    <div>
      <h4 className="text-3xl font-normal leading-normal mt-0 mb-6 mt-8">
        Applications Overview
      </h4>
      <Filters
        name={filters.name}
        jobType={filters.jobType}
        status={filters.status}
        disableFilters={loadingStatus !== LoadingStatus.SUCCEEDED}
      />
      <div className="mt-8">
        <CandidateList candidates={filteredCandidates} />
      </div>
    </div>
  );
}

export default RecruitmentDashboard;
